package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/dvkgroup/go-rpc-auth/internal/endpoints"
	"gitlab.com/dvkgroup/go-rpc-auth/internal/modules"
	"gitlab.com/dvkgroup/go-rpc-auth/internal/storages"
	"gitlab.com/dvkgroup/go-rpc-extension/config"
	"gitlab.com/dvkgroup/go-rpc-extension/db"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/cache"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/db/migrate"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/db/scanner"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/errors"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/godecoder"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/provider"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/responder"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/router"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/server"
	internal "gitlab.com/dvkgroup/go-rpc-extension/infrastructure/service"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/tools/cryptography"
	"gitlab.com/dvkgroup/go-rpc-extension/models"
	grpcauth "gitlab.com/dvkgroup/go-rpc-extension/transport/grpc/auth"
	"gitlab.com/dvkgroup/go-rpc-extension/transport/jrpc/auth"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net/http"
	"net/rpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	RPCsrv   server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Servises *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	//запускаем rpc сервер
	errGroup.Go(func() error {
		err := a.RPCsrv.Serve(ctx)
		if err != nil {
			a.logger.Error("rpc: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.EmailVerifyDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages
	// инициализация сервисов
	services := modules.NewServices(newStorages, components)
	a.Servises = services

	switch a.conf.RPCServer.Type {
	case "grpc":
		authRPC := grpcauth.NewAuthGRPCServer(services.Auth)
		gRPCServer := grpc.NewServer()
		reflection.Register(gRPCServer)
		grpcauth.RegisterAuthServer(gRPCServer, authRPC)
		a.RPCsrv = server.NewGRPCServer(a.conf.RPCServer, gRPCServer, a.logger)
	case "jsonrpc":
		// инициализация сервиса Auth в json RPC
		authRPC := auth.NewAuthJSONRPCServer(services.Auth)
		jsonRPCServer := rpc.NewServer()
		err = jsonRPCServer.Register(authRPC)
		if err != nil {
			a.logger.Fatal("error init auth json RPC", zap.Error(err))
		}
		// инициализация сервера json RPC
		a.RPCsrv = server.NewJSONRPCServer(a.conf.RPCServer, jsonRPCServer, a.logger)
	default:
		a.logger.Fatal("unknown RPC server type", zap.Error(err))
	}

	controllers := modules.NewControllers(services, components)
	// инициализция endpoints
	endpoint := endpoints.NewApiRouter(controllers, components)
	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(endpoint)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
