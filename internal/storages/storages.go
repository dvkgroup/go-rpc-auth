package storages

import (
	vstorage "gitlab.com/dvkgroup/go-rpc-auth/internal/modules/auth/storage"
	"gitlab.com/dvkgroup/go-rpc-extension/db/adapter"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/cache"
)

type Storages struct {
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
