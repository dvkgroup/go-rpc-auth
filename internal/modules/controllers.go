package modules

import (
	acontroller "gitlab.com/dvkgroup/go-rpc-auth/internal/modules/auth/controller"
	ucontroller "gitlab.com/dvkgroup/go-rpc-auth/internal/modules/user/controller"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
