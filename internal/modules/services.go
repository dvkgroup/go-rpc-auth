package modules

import (
	aservice "gitlab.com/dvkgroup/go-rpc-auth/internal/modules/auth/service"
	uservice "gitlab.com/dvkgroup/go-rpc-auth/internal/modules/user/service"
	"gitlab.com/dvkgroup/go-rpc-auth/internal/storages"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	grpcuser "gitlab.com/dvkgroup/go-rpc-extension/transport/grpc/user"
	jrpcuser "gitlab.com/dvkgroup/go-rpc-extension/transport/jrpc/user"
)

type Services struct {
	User interfaces.Userer
	Auth interfaces.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	// инициализация клиента для взаимодействия с сервисом пользователей
	var c interfaces.Userer
	switch components.Conf.RPCServer.Type {
	case "grpc":
		c = grpcuser.NewGRPCClient(components.Conf.UserRPC, components.Logger)
	case "jsonrpc":
		c = jrpcuser.NewJSONRPCClient(components.Conf.UserRPC, components.Logger)
	}

	userClientRPC := uservice.NewUserRPCService(c)

	return &Services{
		User: userClientRPC,
		Auth: aservice.NewAuthService(userClientRPC, storages.Verify, components),
	}
}
